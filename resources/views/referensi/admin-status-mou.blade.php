@extends('base')
@section('title', 'SIMKERMA | Status MOU')
@section('konten')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h6 class="page-title">Data Status MOU</h6>
                {{-- Breadcrumb --}}
            </div>
            <div class="col-md-4">
                <div class="d-flex flex-column flex-md-row justify-content-end align-items-md-center gap-3">
                    <button class="btn btn-primary" type="button" aria-expanded="false"
                            onclick="window.location.href='/export-status'">
                        <i class="mdi mdi-file-export me-2"></i> Ekspor
                    </button>
                    <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal"
                            data-bs-target="#modalTambahStatusMou"><i class="mdi mdi-plus"></i>
                        Tambah Status MoU Baru
                    </button>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card">
        <div class="card-body">
            <div class="row">
                <table id="datatable" class="table table-bordered dt-responsive nowrap"
                       style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Status MoU</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($status as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>
                                <div class="row gap">
                                    <div class="col">
                                        {{-- Edit Button --}}
                                        <button type="button" class="btn btn-warning waves-effect"
                                                data-bs-toggle="modal"
                                                data-bs-target="#edit-modal{{ $item->id }}"><i class="mdi mdi-pencil"
                                                                                        style="color: black"></i>
                                        </button>
                                        {{-- Delete Button --}}
                                        <form action="{{ route('status.destroy', $item->id) }}" method="POST" class="d-inline delete-status">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger waves-effect deleteBtn"><i
                                                    class="mdi mdi-trash-can" style="color: black"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{-- Chart --}}
            </div>
        </div>
    </div>
    {{-- MODAL TAMBAH STATUS MOU --}}
    <div id="modalTambahStatusMou" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog"
         aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('status.store') }}" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel2">Tambah Status MOU Baru</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="col">
                            <div class="col-md-12 pb-3">
                                <label for="nama" class="form-label">Nama Status</label>
                                <input required type="text" class="form-control" id="nama" name="nama" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    @foreach($status as $item)
    {{-- MODAL EDIT STATUS MOU --}}
    <div id="edit-modal{{ $item->id }}" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog"
         aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('status.update', $item->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel2">Edit Jenis MOU</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="col">
                            <div class="col-md-12 pb-3">
                                <label for="nama" class="form-label">Nama Jenis Mitra</label>
                                <input type="text" value="{{ $item->nama }}" class="form-control" id="nama" value="" name="nama"
                                       required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    @endforeach
@endsection

@push('script')
    <script>
        const allStatus = document.querySelectorAll('.delete-status');
        allStatus.forEach(function (status) {
            status.addEventListener('submit', function (e) {
                e.preventDefault();
                Swal.fire({
                    title: 'Apakah Anda yakin?',
                    text: "Anda tidak akan dapat mengembalikan ini!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Ya, Hapus Data!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        status.submit();
                    }
                });
            })
        });
    </script>
@endpush
