    {{-- Sidebar diberi logika untuk masing masing user --}}
    {{-- User superadmin : punya manajemen user --}}
    {{-- admin : tidak punya manajemen user --}}
    {{-- prodi : Input & View MOU --}}
    <div class="vertical-menu">

        <div data-simplebar class="h-100">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <ul class="metismenu list-unstyled" id="side-menu">
                    @auth
                        @if (Auth::user()->role === 'User')
                            <li>
                                <a href="javascript: void(0);" class="has-arrow waves-effect">
                                    <i class="ti-pie-chart"></i>
                                    <span>Data Kerja Sama</span>
                                </a>
                                <ul class="sub-menu" aria-expanded="false">
                                    <li><a href="/kerjasama">Semua Kerja Sama</a></li>
                                    <li><a href="kerjasama-Aktif">Kerja Sama Aktif</a></li>
                                    <li><a href="/kerjasama-Berakhir">Kerja Sama Berakhir</a></li>
                                    <li><a href="/kerjasama-AkanBerakhir">Kerja Sama Akan Berakhir</a></li>
                                </ul>
                            </li>
                        @endif
                    @endauth

                    @auth
                        @if (Auth::user()->role === 'Admin' || Auth::user()->role === 'Super Admin')
                            <li>
                                <a href="{{ route('dashboard') }}" class="waves-effect">
                                    <i class="ti-home"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript: void(0);" class="has-arrow waves-effect">
                                    <i class="ti-pie-chart"></i>
                                    <span>Data Kerja Sama</span>
                                </a>
                                <ul class="sub-menu" aria-expanded="false">
                                    <li><a href="/kerjasama">Semua Kerja Sama</a></li>
                                    <li><a href="kerjasama-Aktif">Kerja Sama Aktif</a></li>
                                    <li><a href="/kerjasama-Berakhir">Kerja Sama Berakhir</a></li>
                                    <li><a href="/kerjasama-AkanBerakhir">Kerja Sama Akan Berakhir</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="/template" class="waves-effect">
                                    <i class="ti-receipt"></i>
                                    <span>Template MoU</span>
                                </a>
                            </li>

                            <li>
                                <a href="/mitra" class="waves-effect">
                                    <i class="far fa-handshake"></i>
                                    <span>Mitra</span>
                                </a>
                            </li>

                            @auth
                                @if (Auth::user()->role === 'Super Admin')
                                    <li>
                                        <a href="/manajemen-pengguna" class="waves-effect">
                                            <i class="ti-user"></i>
                                            <span>Pengguna</span>
                                        </a>
                                    </li>
                                @endif
                            @endauth

                            <li>
                                <a href="javascript: void(0);" class="has-arrow waves-effect">
                                    <i class="ti-view-grid"></i>
                                    <span>Referensi</span>
                                </a>
                                <ul class="sub-menu" aria-expanded="false">
                                    <li><a href="/jenis-mitra">Jenis Mitra</a></li>
                                    <li><a href="/status-mou">Status MoU</a></li>
                                    <li><a href="/program-studi">Program Studi</a></li>
                                </ul>
                            </li>
                        @endif
                    @endauth

                </ul>
            </div>
            <!-- Sidebar -->
        </div>
    </div>
    <!-- Left Sidebar End -->
