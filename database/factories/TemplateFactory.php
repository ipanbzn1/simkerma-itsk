<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Template>
 */
class TemplateFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        Storage::makeDirectory('Data-Template');
        return [
            'nama' => 'Bahasa ' . fake()->country() . ' (MOU)',
            'file' => 'Data-Template/' . fake()->file(public_path('Data-Template'), Storage::path('Data-Template'), false),
            'is_active' => true,
            'user_id' => 1,
        ];
    }
}
