<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\JenisMitra;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            UserSeeder::class,
            RoleSeeder::class,
            StatusKerjasamaSeeder::class,
            JenisMitraSeeder::class,
            MitraSeeder::class,
            KerjasamaSeeder::class,
            ProdiSeeder::class,
            TemplateSeeder::class,
        ]);
    }
}
