<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class MitraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
{
    // Gunakan Faker untuk menghasilkan data dummy
    $faker = Faker::create('id_ID'); // Set locale ke bahasa Indonesia

    for ($i = 0; $i < 15; $i++) {
        DB::table('mitra')->insert([
            'nama_mitra' => $faker->company,
            'jenis_mitra' => $faker->randomElement(['Instansi Pemerintah, BUMN Dan/Atau BUMD', 'Institusi Pendidikan', 'Perusahaan', 'Organisasi', 'Rumah Sakit', 'UMKM']),
            'penanggung_jawab' => $faker->name,
            'email' => $faker->email,
            'no_telp' => substr($faker->phoneNumber, 0, 15), // Membatasi nomor telepon hingga 15 karakter
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}

}
