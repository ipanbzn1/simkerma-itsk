<?php

use App\Http\Controllers\KerjaSamaController;
use App\Http\Controllers\MitraController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/get-mitra', [MitraController::class, 'get_data'])->name('get.mitra');
Route::post('/send-datakerjasama', [KerjaSamaController::class, 'store'])->name('store-kerjasama');
