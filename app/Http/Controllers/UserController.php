<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Prodi;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $prodi = Prodi::all();
        return view('pengguna.manajemen-pengguna', compact('users', 'prodi'));
    }

    public function exportUsers()
    {

        $users = User::all();
        $no = 1;

        (new FastExcel($users))->export('data-users.xlsx', function ($user) use (&$no) {
            return [
                'No' => $no++,
                'Nama' => $user->nama,
                'Email' => $user->email,
                'Prodi' => $user->prodi,
                'Role' => $user->role,
            ];
        });

        return response()->download('data-users.xlsx')->deleteFileAfterSend();
    }

    public function show(User $user)
    {
        return view('pengguna.manajemen-pengguna-show', compact('user'));
    }

    public function create()
    {
        return view('pengguna.manajemen-pengguna-create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'prodi' => $request->role == 'User' || $request->role == 'Admin' ? 'required|max:255' : 'nullable|max:255',
            'role' => 'required|max:255',
        ], [
            'nama.required' => 'Nama wajib diisi.',
            'nama.max' => 'Nama tidak boleh lebih dari :max karakter.',
            'email.required' => 'Email wajib diisi.',
            'email.email' => 'Format email tidak valid.',
            'email.unique' => 'Email sudah digunakan.',
            'password.required' => 'Password wajib diisi.',
            'prodi.required' => 'Program Studi wajib diisi.',
            'prodi.max' => 'Program Studi tidak boleh lebih dari :max karakter.',
            'role.required' => 'Role wajib diisi.',
            'role.max' => 'Role tidak boleh lebih dari :max karakter.',
        ]);

        $validatedData['is_active'] = true;
        $validatedData['password'] = bcrypt($request->password);

        User::create($validatedData);

        return response()->json(['message' => 'Pengguna berhasil ditambah!'], 200);
    }

    public function edit(User $user)
    {
        return view('pengguna.manajemen-pengguna-edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'prodi' => $request->role == 'User' || $request->role == 'Admin' ? 'required|max:255' : 'nullable|max:255',
            'role' => 'required|max:255',
        ], [
            'nama.required' => 'Nama wajib diisi.',
            'nama.max' => 'Nama tidak boleh lebih dari :max karakter.',
            'email.required' => 'Email wajib diisi.',
            'email.email' => 'Format email tidak valid.',
            'email.unique' => 'Email sudah digunakan.',
            'prodi.max' => 'Program Studi tidak boleh lebih dari :max karakter.',
            'role.required' => 'Role wajib diisi.',
            'role.max' => 'Role tidak boleh lebih dari :max karakter.',
        ]);

        $user->update($validatedData);

        return response()->json(['message' => 'Pengguna berhasil diedit!'], 200);
    }

    public function updateStatus(Request $request)
    {
        $userId = $request->input('userId');

        $user = User::findOrFail($userId);
        $user->is_active = !$user->is_active;
        $user->save();

        return response()->json(['success' => true]);
    }
}
