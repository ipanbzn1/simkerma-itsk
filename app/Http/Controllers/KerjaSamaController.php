<?php

namespace App\Http\Controllers;

use App\Models\DataKerjasama;
use App\Models\Mitra;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class KerjaSamaController extends Controller
{
    public function store(Request $request){
        try{
            $validatedData = $request->validate([
                'penanggung_jawab' => 'required|max:100',
                'email' => 'required|email',
                'no_telp' => 'required|max:15',
                'nama_mitra' => 'required|max:100',
                'deskripsi' =>  'required|max:255',
                'tanggal_mou' => 'required|date',
                'tanggal_akhir' => 'required|date',
                'file' => 'required|file',
                'no_surat_mitra' => 'required|max:100'
            ]);
            
            if(strtotime($request->input('tanggal_mou')) > strtotime($request->input('tanggal_akhir'))){
                throw new \Exception('Tanggal Awal Tidak Boleh Melewati Tanggal Akhir',400);
            }
            
            $file =  $request->file('file');
            $originalFilename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $filename = hash('sha256', time().$originalFilename).'.'.$extension;
            $fileExt = $file->getClientMimeType();
            if($fileExt != 'application/pdf'){
                throw new \Exception('Format Dokumen Tidak Sesuai', 400);
            }
            $file->move(public_path('Data-Kerjasama'), $filename);
            
            $validatedData['file']= $filename;
            
            $validatedData['mitra_id']= intval($request->nama_mitra) ;
            if(!intval($validatedData['nama_mitra'])){
                Mitra::create($validatedData);
                $id = Mitra::where('nama_mitra',$request->nama_mitra)->pluck('id');
                $validatedData['mitra_id']= $id[0];
            }
            DataKerjasama::create($validatedData);
            
            return  response()->json(['success' => 'untuk informasi lebih lanjut hubungi 0811-3229-9222']);
        }
        catch(\Illuminate\Validation\ValidationException $e) {
            $errorMessages = implode(" ", Arr::flatten($e->errors()));
            return response()->json(['error'=> $errorMessages]);
        }
        catch(\Exception $e){
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function storeAdmin(Request $request){
        try{
            $validator = Validator::make($request->all(),[
                'mitra_id' => 'required',
                'deskripsi' =>  'required|max:255',
                'tanggal_mou' => 'required|date',
                'tanggal_akhir' => 'required|date',
                'files' => 'required|file',
                'no_surat_mitra' => 'required|max:50'
            ]);
            $request->merge(['mitra_id' => intval($request->mitra_id)]);
            if ($validator->fails()){
                throw new \Exception('Inputan Tidak Lengkap!', 400);
            }
            if(strtotime($request->input('tanggal_mou')) > strtotime($request->input('tanggal_akhir'))){
                throw new \Exception('Tanggal Awal Tidak Boleh Melewati Tanggal Akhir',400);
            }
            //proses simpan file
            $file =  $request->file('files');
            $originalFilename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $filename = hash('sha256', time().$originalFilename).'.'.$extension;
            $file->move(public_path('Data-Kerjasama'), $filename); 
            $request['file'] = $filename;
            $request['user_id'] = Auth::user()->id;
            //Save Data ke DB
            DataKerjasama::create($request->all());
            return  redirect('/kerjasama')->with('success', 'Data berhasil disimpan');
        }
        catch(\Exception $e){
            return redirect('/kerjasama')->with('error',$e->getMessage())->withInput();
        }
    }
    public function update(Request $request,$id){
        $inputData = $request->all();

        foreach ($inputData as $key => $value) {
            if ($value === null) {
                $inputData[$key] = '';
            }
        }
        
        $find = DataKerjasama::findOrFail($id);
        
        if($request->file){
            $filePath = public_path('Data-Kerjasama/'. $find->file);
            $file =  $request->file('file');
            $originalFilename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $filename = hash('sha256', time().$originalFilename).'.'.$extension;
            $fileExt = $file->getClientMimeType();
            if($fileExt != 'application/pdf'){
                return redirect('/kerjasama')->with('error','Format Dokumen Tidak Sesuai');
            }
            if(File::exists($filePath)){
                File::delete($filePath);
            }
            $file->move(public_path('Data-Kerjasama'), $filename);
            
            $inputData['file']= $filename;
        }

        $find->update($inputData);
        
        return redirect('/kerjasama')->with('success', 'Data berhasil diperbarui');

    }
    public function delete(Request $request){
        $id = $request->input('id');
        $find = DataKerjasama::findOrFail($id);
        if($find){
            //deleting file first 
            $filePath = public_path('Data-Kerjasama/'. $find->file);
            if(File::exists($filePath)){
                File::delete($filePath);
            }
            //deleting data in db
            $find->delete();
            return redirect('/kerjasama')->with('success','Data Berhasil Dihapus');
        }
        return redirect('/kerjasama')->with('error','error data tidak ditemukan');

    }
    public function download($file){
        $filePath = public_path('Data-Kerjasama/' . $file);
        if(file_exists($filePath)){
            return Response::download($filePath);
        }else{
            abort(404);
        }
    }
    // export all
    public function export(){
        if(Auth::user()->role == 'Admin' || Auth::user()->role == 'User'){
            $prodi = Auth::user()->prodi;
            $Datakerjasama = DB::table('data_kerjasama as dk')
                                ->orderBy('dk.tanggal_mou', 'desc')
                                ->join('users as u', 'dk.user_id', '=', 'u.id')
                                ->join('mitra', 'dk.mitra_id', '=', 'mitra.id')
                                ->select('dk.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp', 'u.prodi as prodi_user')
                                ->where('u.prodi', $prodi)
                                ->get();
        }
        else{
            $Datakerjasama = DataKerjasama::orderBy('created_at', 'desc')
                                            ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                            ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp')
                                            ->get();
        }
        $no = 1;

        (new FastExcel($Datakerjasama))->export('data-Kerjasama.xlsx', function ($item) use (&$no) {
            return [
                'No' => $no++,
                'Nama Penanggung Jawab' => $item->penanggung_jawab,
                'Email' => $item->email,
                'Nama Kegiatan' => $item->deskripsi,
                'Nama Mitra' => $item->nama_mitra,
                'Jenis Mitra' => $item->jenis_mitra,
                'Tanggal Mulai' => $item->tanggal_mou,
                'Tanggal Berakhir' => $item->tanggal_akhir,
                'Nomor Surat Mitra' => $item->no_surat_mitra,
                'Nomor Surat Instansi' => $item->no_surat_instansi,
                'Jenis Dokumen' => $item->jenis_dokumen,
                'Status Pengajuan' => $item->status
            ];
        });
        return response()->download('data-kerjasama.xlsx')->deleteFileAfterSend();
    }
    public function exportDataBerakhir(){
        if(Auth::user()->role == 'Admin' || Auth::user()->role == 'User'){
            $prodi = Auth::user()->prodi;
            $data = DB::table('data_kerjasama as dk')
                                ->orderBy('dk.tanggal_mou', 'desc')
                                ->join('users as u', 'dk.user_id', '=', 'u.id')
                                ->join('mitra', 'dk.mitra_id', '=', 'mitra.id')
                                ->select('dk.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp', 'u.prodi as prodi_user')
                                ->where('u.prodi', $prodi)
                                ->get();  
        }
        else{
            $data = DataKerjasama::orderBy('created_at', 'desc')
                                    ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                    ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp')
                                    ->get();
        }
        $Datakerjasama = $data->filter(function($item){
            $tanggalAkhir = Carbon::parse($item->tanggal_akhir);
            return $tanggalAkhir->isPast();
        });
        $no = 1;

        (new FastExcel($Datakerjasama))->export('data-Kerjasama.xlsx', function ($item) use (&$no) {
            return [
                'No' => $no++,
                'Nama Penanggung Jawab' => $item->penanggung_jawab,
                'Email' => $item->email,
                'Nama Kegiatan' => $item->deskripsi,
                'Nama Mitra' => $item->nama_mitra,
                'Jenis Mitra' => $item->jenis_mitra,
                'Tanggal Mulai' => $item->tanggal_mou,
                'Tanggal Berakhir' => $item->tanggal_akhir,
                'Nomor Surat Mitra' => $item->no_surat_mitra,
                'Nomor Surat Instansi' => $item->no_surat_instansi,
                'Jenis Dokumen' => $item->jenis_dokumen,
                'Status Pengajuan' => $item->status
            ];
        });
        return response()->download('data-kerjasama.xlsx')->deleteFileAfterSend();
    }
    public function exportDataAktif(){
        if(Auth::user()->role == 'Admin' || Auth::user()->role == 'User'){
            $prodi = Auth::user()->prodi;
            $data = DB::table('data_kerjasama as dk')
                                ->orderBy('dk.tanggal_mou', 'desc')
                                ->join('users as u', 'dk.user_id', '=', 'u.id')
                                ->join('mitra', 'dk.mitra_id', '=', 'mitra.id')
                                ->select('dk.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp', 'u.prodi as prodi_user')
                                ->where('u.prodi', $prodi)
                                ->get();  
        }
        else{
            $data = DataKerjasama::orderBy('created_at', 'desc')
                                    ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                    ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp')
                                    ->get();
        }
        $Datakerjasama = $data->filter(function($item){
            $tanggalAkhir = Carbon::parse($item->tanggal_akhir);
            return $tanggalAkhir->isFuture();
        });
        $no = 1;

        (new FastExcel($Datakerjasama))->export('data-Kerjasama.xlsx', function ($item) use (&$no) {
            return [
                'No' => $no++,
                'Nama Penanggung Jawab' => $item->penanggung_jawab,
                'Email' => $item->email,
                'Nama Kegiatan' => $item->deskripsi,
                'Nama Mitra' => $item->nama_mitra,
                'Jenis Mitra' => $item->jenis_mitra,
                'Tanggal Mulai' => $item->tanggal_mou,
                'Tanggal Berakhir' => $item->tanggal_akhir,
                'Nomor Surat Mitra' => $item->no_surat_mitra,
                'Nomor Surat Instansi' => $item->no_surat_instansi,
                'Jenis Dokumen' => $item->jenis_dokumen,
                'Status Pengajuan' => $item->status
            ];
        });
        return response()->download('data-kerjasama.xlsx')->deleteFileAfterSend();
    }
    public function exportDataAkanBerakhir(){
        $hariIni = Carbon::now();
        $tujuhHariKedepan = $hariIni->copy()->addDays(7);
        if(Auth::user()->role == 'Admin' || Auth::user()->role == 'User'){
            $prodi = Auth::user()->prodi;
            $Datakerjasama = DB::table('data_kerjasama as dk')
                                ->orderBy('dk.tanggal_mou', 'desc')
                                ->join('users as u', 'dk.user_id', '=', 'u.id')
                                ->join('mitra', 'dk.mitra_id', '=', 'mitra.id')
                                ->select('dk.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp', 'u.prodi as prodi_user')
                                ->where('u.prodi', $prodi)
                                ->where('tanggal_akhir','>', $hariIni)
                                ->where('tanggal_akhir','<',$tujuhHariKedepan)
                                ->get();  
        }
        else{
            $Datakerjasama = DataKerjasama::orderBy('created_at', 'desc')
                                            ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                            ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp')
                                            ->where('tanggal_akhir','>', $hariIni)
                                            ->where('tanggal_akhir','<',$tujuhHariKedepan)
                                            ->get();
        }
        $no = 1;

        (new FastExcel($Datakerjasama))->export('data-Kerjasama.xlsx', function ($item) use (&$no) {
            return [
                'No' => $no++,
                'Nama Penanggung Jawab' => $item->penanggung_jawab,
                'Email' => $item->email,
                'Nama Kegiatan' => $item->deskripsi,
                'Nama Mitra' => $item->nama_mitra,
                'Jenis Mitra' => $item->jenis_mitra,
                'Tanggal Mulai' => $item->tanggal_mou,
                'Tanggal Berakhir' => $item->tanggal_akhir,
                'Nomor Surat Mitra' => $item->no_surat_mitra,
                'Nomor Surat Instansi' => $item->no_surat_instansi,
                'Jenis Dokumen' => $item->jenis_dokumen,
                'Status Pengajuan' => $item->status
            ];
        });
        return response()->download('data-kerjasama.xlsx')->deleteFileAfterSend();
    }
}
