<?php

namespace App\Http\Controllers;

use App\Models\DataKerjasama;
use App\Models\Mitra;
use App\Models\StatusKerjasama;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function showAdmin(){
        return view ('dashboard/admin-dashboard');
    }

    public function showKerjasama(){
        if(Auth::user()->role === 'Admin' || Auth::user()->role === 'User' ){
            $prodi = Auth::user()->prodi;
            $dataKerjasama = DB::table('data_kerjasama as dk')
                                ->orderBy('dk.tanggal_mou', 'desc')
                                ->join('users as u', 'dk.user_id', '=', 'u.id')
                                ->join('mitra', 'dk.mitra_id', '=', 'mitra.id')
                                ->select('dk.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp', 'u.prodi as prodi_user')
                                ->where('u.prodi', $prodi)
                                ->get();
        }
        else{
            $dataKerjasama = DataKerjasama::orderBy('tanggal_mou', 'desc')
                                            ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                            ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp')
                                            ->get();
        }
        $jenis = 'show';
        $Mitra = Mitra::all();
        $status = StatusKerjasama::get();
        return view ('data-kerja-sama/admin-data-kerjasama', compact('dataKerjasama','jenis','Mitra','status'));
    }
    public function ShowKerjasamaBerakhir(){
        if(Auth::user()->role === 'Admin' || Auth::user()->role === 'User' ){
            $prodi = Auth::user()->prodi;
            $data = DB::table('data_kerjasama as dk')
                                ->orderBy('dk.tanggal_mou', 'desc')
                                ->join('users as u', 'dk.user_id', '=', 'u.id')
                                ->join('mitra', 'dk.mitra_id', '=', 'mitra.id')
                                ->select('dk.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp', 'u.prodi as prodi_user')
                                ->where('u.prodi', $prodi)
                                ->get();
        }
        else{
            $data = DataKerjasama::orderBy('tanggal_mou', 'desc')
                                            ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                            ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp')
                                            ->get();
        }
        $dataKerjasama = $data->filter(function($item){
            $tanggalAkhir = Carbon::parse($item->tanggal_akhir);
            return $tanggalAkhir->isPast();
        });
        $jenis = "Berakhir";
        $Mitra = Mitra::all();
        $status = StatusKerjasama::all();
        return view('data-kerja-sama/admin-data-kerjasama', compact('dataKerjasama', 'jenis','Mitra','status'));
    }
    public function ShowKerjaSamaAktif(){
        if(Auth::user()->role === 'Admin' || Auth::user()->role === 'User' ){
            $prodi = Auth::user()->prodi;
            $data = DB::table('data_kerjasama as dk')
                                ->orderBy('dk.tanggal_mou', 'desc')
                                ->join('users as u', 'dk.user_id', '=', 'u.id')
                                ->join('mitra', 'dk.mitra_id', '=', 'mitra.id')
                                ->select('dk.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp', 'u.prodi as prodi_user')
                                ->where('u.prodi', $prodi)
                                ->get();
        }
        else{
            $data = DataKerjasama::orderBy('tanggal_mou', 'desc')
                                            ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                            ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp')
                                            ->get();
        }
        $dataKerjasama = $data->filter(function($item){
            $tanggalAkhir = Carbon::parse($item->tanggal_akhir);
            return $tanggalAkhir->isFuture();
        });
        $jenis = "Aktif";
        $Mitra = Mitra::all();
        $status = StatusKerjasama::all();
        return view('data-kerja-sama/admin-data-kerjasama',compact('dataKerjasama', 'jenis','Mitra','status'));
    }
    public function ShowKerjasamaAkanBerakhir(){
        $hariIni = Carbon::now();
        $tujuhHariKedepan = $hariIni->copy()->addDays(7);
        if(Auth::user()->role === 'Admin' || Auth::user()->role === 'User'){
            $prodi = Auth::user()->prodi;
            $dataKerjasama = DB::table('data_kerjasama as dk')
                                ->orderBy('dk.tanggal_mou', 'desc')
                                ->join('users as u', 'dk.user_id', '=', 'u.id')
                                ->join('mitra', 'dk.mitra_id', '=', 'mitra.id')
                                ->select('dk.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp', 'u.prodi as prodi_user')
                                ->where('u.prodi', $prodi)
                                ->where('tanggal_akhir','>', $hariIni)
                                ->where('tanggal_akhir','<',$tujuhHariKedepan)
                                ->get();
        }
        else{
            $dataKerjasama = DataKerjasama::orderBy('tanggal_akhir', 'asc')
                                            ->join('mitra', 'data_kerjasama.mitra_id', '=', 'mitra.id')
                                            ->select('data_kerjasama.*', 'mitra.nama_mitra', 'mitra.jenis_mitra', 'mitra.penanggung_jawab', 'mitra.email', 'mitra.no_telp')
                                            ->where('tanggal_akhir','>', $hariIni)
                                            ->where('tanggal_akhir','<',$tujuhHariKedepan)
                                            ->get();
        }
        $jenis = "Akan Berakhir";
        $Mitra = Mitra::all();
        $status = StatusKerjasama::all();
        return view('data-kerja-sama/admin-data-kerjasama', compact('dataKerjasama', 'jenis','Mitra','status'));
    }
}
