<?php

namespace App\Http\Controllers;

use App\Models\Prodi;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;

class ProdiController extends Controller
{
    public function index()
    {
        $prodi = DB::table('prodi')->get()->map(function ($prodi) {
            $jumlahUser = User::where('prodi', $prodi->nama)->count();
            return [
                'id' => $prodi->id,
                'nama' => $prodi->nama,
                'jumlahUser' => $jumlahUser,
            ];
        });

        return view('referensi.prodi', compact('prodi'));
    }

    public function exportProdi()
    {
        $prodi = Prodi::all();
        $no = 1;

        (new FastExcel($prodi))->export('data-prodi.xlsx', function ($prodi) use (&$no) {
            return [
                'No' => $no++,
                'Nama' => $prodi->nama,
            ];
        });

        return response()->download('data-prodi.xlsx')->deleteFileAfterSend();
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
        ]);

        Prodi::create($validatedData);

        return redirect()->route('prodi.index')->with('success', 'Program studi berhasil ditambah!');
    }

    public function update(Request $request, Prodi $prodi)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:50',
        ]);

        $userCount = User::where('prodi', $prodi->nama)->count();
        if ($userCount > 0) {
            return redirect()->route('prodi.index')->with('error', 'Program studi tidak bisa diedit karena memiliki user');
        }

        $prodi->update($validatedData);

        return redirect()->route('prodi.index')->with('success', 'Program studi berhasil diubah!');
    }

    public function destroy(Prodi $prodi)
    {
        $userCount = User::where('prodi', $prodi->nama)->count();
        if ($userCount > 0) {
            return redirect()->route('prodi.index')->with('error', 'Program studi tidak bisa dihapus karena memiliki user');
        }

        $prodi->delete();

        return redirect()->route('prodi.index')->with('success', 'Program studi berhasil dihapus');
    }
}
