<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $fileValidation = ['file', 'mimes:docx,pdf'];
        if($this->method() == 'POST'){
            array_unshift($fileValidation, 'required');
        }
        return [
            'nama' => 'required|string|max:100',
            'file' => $fileValidation,
            'is_active' => 'required'
        ];
    }
}
