<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mitra extends Model
{
    protected $table = 'mitra';

    protected $fillable = [
        'nama_mitra',
        'jenis_mitra',
        'penanggung_jawab',
        'email',
        'no_telp',
    ];

    public function dataKerjasama()
    {
        return $this->hasMany(DataKerjasama::class);
    }
}
